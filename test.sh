#!/bin/zsh

echo "%include \"colon.inc\"

section .data
colon \"third word\", third_word
db \"third word explanation\", 0

colon \"second word\", second_word
db \"second word explanation\", 0 

colon \"first word\", first_word
db \"first word explanation\", 0 
" > words.inc

touch main.asm

make init

test_program() {
    input="$1"
    expected_output="$2"
    output=`echo "$input" | ./main 2> err.txt`
    if [ "$output" = "$expected_output" ]
    then
      echo "Test passed"
    else
      echo "Test failed. Expected: '$expected_output', got: '$output'"
    fi

}


test_program "first word" "first word explanation"
test_program "popa" ""
test_program "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" ""


rm -f input.txt output.txt expected_output.txt
exit 0