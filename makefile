.PHONY: main
init: main

main: lib.o dict.o main.o
	@ld -o main lib.o dict.o main.o


# %.o: %.asm
# 	@nasm -f elf64 -o %.o %.asm
# по какой то причине не работает да и ладно

dict.o: dict.asm lib.inc
	@nasm -f elf64 -o dict.o dict.asm

main.o: main.asm lib.inc words.inc
	@nasm -f elf64 -o main.o main.asm

lib.o: lib.asm
	@nasm -f elf64 -o lib.o lib.asm

test: main.asm
	@./test.sh