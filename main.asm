%include "lib.inc"
%include "dict.inc"
%include "words.inc"
%define BUF_SIZE 255

global _start



section .bss
  word_buf: resb BUF_SIZE

section .rodata
    not_foud_message: db "not found",0
    input_error_message: db "error: input error",0

section .text

_start:
    mov rdi, word_buf
    mov rsi, BUF_SIZE
    call read_string
    test rax, rax
    jz .io_err
    mov rdi, rax
    mov rsi, first_word
    call find_word
    test rax, rax
    jz .not_found
    mov rdi, rax
    call print_string
    ; call print_newline
    ; jmp _start
    jmp .end
.io_err:
    mov rdi, input_error_message
    call print_error
    mov rdi, -1
    jmp .end
.not_found:
    mov rdi, not_foud_message
    call print_error
    ; call print_newline
    ; jmp _start
    mov rdi, -1
.end:
    call exit