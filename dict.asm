%include "lib.inc"
%define qword 8

global find_word

find_word:
; Указатель на нуль-терминированную строку. rdi
; Указатель на начало словаря.              rsi
    push r12
    push r13
    push r14            ; save next_pointer
    mov r12, rdi
    mov r13, rsi        ; r13 - key
.loop_find:
    mov r14, [r13]      ; next pointer
    add r13, qword
    mov rdi, r12
    mov rsi, r13
    call string_equals
    test rax, rax       ; next if not eq
    jz .next
    mov rdi, r13
    call string_length
    add r13, rax        ; skip keyword
    inc r13             ; skip null-terminator
    mov rax, r13
    jmp .end
.next:
    test r14, r14       ; if 0 then end of dict
    jz .not_found
    mov r13, r14
    jmp .loop_find
.not_found:
    xor rax, rax
    jmp .end
.end:
    pop r14
    pop r13
    pop r12
    ret



    
